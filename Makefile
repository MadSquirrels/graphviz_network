DOT=graph.png

all: $(DOT)

%.png:%.dot
	dot -Tpng $< -o $@

clean:
	rm -f $(DOT)
